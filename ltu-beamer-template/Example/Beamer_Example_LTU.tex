% Example presentation using the LTU Beamer add-on.
%
% Created: June 2012, by Johan E. Carlson
% Last modified: June 18, 2012, by Johan E. Carlson


% Pick one of the \documentclass declarations below to creare either a Swedish or an English
% presentation, with or without page numbers. 
% For additional class options, see the Beamer documentation. 

\documentclass[t,eng]{beamer}
%\documentclass[t,sv]{beamer}
%\documentclass[t,eng,nopagenum]{beamer}
%\documentclass[t,sv,nopagenum]{beamer}
%\documentclass[t,eng,handouts]{beamer}
%\documentclass[t,eng,shownotes]{beamer}

% Include the LTU theme and add-on functions
\usetheme{LTU}
%\usepackage{EU_logo}
%\usepackage{HLRC_logo}

%===================================================================================
% Start actual presentation
%===================================================================================

\title{An LTU \LaTeX\ template for slides using Beamer}
\author{Johan E.\ Carlson}
\institute{Dept.\ of Computer Science, Electrical and Space Engineering\\
Lule� University of Technology}
\date{June 18, 2012}

\begin{document}
%-----------------------------------------------------------------------------------
% Title and table of contents stuff
%-----------------------------------------------------------------------------------
% Generate title page
\begin{frame}
	\titlepage
\end{frame}

% Generate a table of contents and show it all
\begin{frame}{Outline}
	\tableofcontents
\end{frame}

% Re-display table of contents at the start of each section, with some nice shading.
% Comment the next frame if you do not want to re-display the TOC.
\AtBeginSection[]
{
\begin{frame}{Outline}
	\tableofcontents[currentsection]
\end{frame}
}

%===================================================================================
% Start of "normal slides"
%===================================================================================

% Declare sections and subsections to determine what shows up on the TOC slide
\section{Getting started}  

%-----------------------------------------------------------------------------------
\begin{frame}{Getting started}
\begin{itemize}
	\item The Beamer document class provides a variety of class options. See the Beamer
documentation  for more information on these.
	\item Make sure you have a working Beamer document class installed.
	\item This add-on provides some additional class options:
	\begin{itemize}
		\item Language selection: Swedish or English (controls the template background)
		\item Page numbering: Turning off slide numbering (default is with numbering enabled).
		\item Show slide notes (see Beamer documentation for more information)
		\item Handout view -- Typeset in handout format, \\ with room for notes.
	\end{itemize}
	\item The next slide shows how to do this.
\end{itemize}
\end{frame}

%-----------------------------------------------------------------------------------
\begin{frame}[fragile]{Getting started}
\begin{codeblock}{0.85}{Document class options, use one of these}
\begin{verbatim}
 \documentclass[t,eng]{beamer}
 \documentclass[t,sv]{beamer}
 \documentclass[t,eng,nopagenum]{beamer}
 \documentclass[t,sv,nopagenum]{beamer}
 \documentclass[t,eng,handouts]{beamer}
 \documentclass[t,eng,shownotes]{beamer}
\end{verbatim}
\end{codeblock}
The [t] option means all slides are aligned to top of page by default. See Beamer documentation for other options.
\end{frame}

%-----------------------------------------------------------------------------------
\begin{frame}[fragile]{Getting started}
There are two additional packages included for adding logotypes, one for projects funded by EU and one for projects funded by HLRC (Hjalmar Lundbohm Research Centre). To add either one of these logos to the presentation, include one of the following commands 
\begin{itemize}
	\item \texttt{$\backslash$usepackage\{EU\_logo\}}
	\item \texttt{$\backslash$usepackage\{HLRC\_logo\}}
\end{itemize}
If you want some other logo, check the code of either of the two packages above (.sty file) and create a new one, adding the logo of your choice.
\end{frame}

%-----------------------------------------------------------------------------------
\begin{frame}[fragile]{Getting started}
\begin{codeblock}{0.85}{Setting up the title page}
\begin{verbatim}
 \title{Title of the presentation}
 \author{Your name}
 \institute{Your address/affiliation}
 \date{The date}
\end{verbatim}
\end{codeblock}
Put this block before the \texttt{$\backslash$begin\{document\}} declaration.
\end{frame}

%-----------------------------------------------------------------------------------
\begin{frame}[fragile]{Getting started}
\begin{codeblock}{0.85}{Displaying the title page}
\begin{verbatim}
 \begin{frame} 
    \titlepage
 \end{frame}
\end{verbatim}
\end{codeblock}
\end{frame}

%-----------------------------------------------------------------------------------
\begin{frame}{Getting started}
\begin{itemize}
	\item Beamer can set up the outline slide, i.e.\ a table of contents (TOC) automatically.
	\item Use \texttt{$\backslash$section} and \texttt{$\backslash$subsection} declarations in the document to define the TOC entries (see source of this document for an example).
	\item The next slides will show how to include the TOC in your presentation.
\end{itemize}
\end{frame}

%-----------------------------------------------------------------------------------
\begin{frame}[fragile]{Getting started}
\begin{codeblock}{0.85}{Displaying the TOC}
\begin{verbatim}
 % Generate a table of contents and show it all
 \begin{frame}{Outline}
    \tableofcontents
 \end{frame}
\end{verbatim}
\end{codeblock}
\end{frame}

%-----------------------------------------------------------------------------------
\begin{frame}[fragile]{Getting started}
\begin{codeblock}{0.85}{Re-displaying the TOC for each section}
\begin{verbatim}
 % Make the TOC re-occur at the start of each 
 % section, with all other sections shaded

 \AtBeginSection[]
 {
  \begin{frame}{Outline}
     \tableofcontents[currentsection]
  \end{frame}
 }
\end{verbatim}
\end{codeblock}
\end{frame}

%-----------------------------------------------------------------------------------
\section{Creating slides}
\subsection{Simple slides}
\begin{frame}{Creating slides}
  \begin{itemize}
	 \item The next section of slides will give some examples on how to create typical, simple slides, with bullet lists, numbered lists, single figures, equations, etc.
	 \item But first, let's look at how an empty slide is generated.
  \end{itemize}
\end{frame}

%-----------------------------------------------------------------------------------
\begin{frame}[fragile]{A simple empty slide}
\begin{codeblock}{.75}{Code example, empty slide}
\begin{verbatim}
 \begin{frame}{An empty slide}
    % This is where you print the contents.
 \end{frame}
\end{verbatim}
\end{codeblock}
\begin{codeblock}{.8}{Code example, empty slide}
\begin{verbatim}
 \begin{frame}[fragile]{An empty slide}
    % This is where you print the contents.
    % The "fragile" option is required if the
    % slide should contain verbatim environments
 \end{frame}
\end{verbatim}
\end{codeblock}

\end{frame}

%-----------------------------------------------------------------------------------
\begin{frame}[fragile]{Bullet lists}
\begin{columns}[t]
	\begin{column}{0.5\textwidth}
\begin{codeblock}{1}{\LaTeX\ code}
\begin{verbatim}
\begin{itemize}
  \item Item 1
  \item Item 2
  \begin{itemize}
     \alert{\item Sub-item 1}
     \item Sub-item 2
  \end{itemize}
\end{itemize}
\end{verbatim}
\end{codeblock}
	\end{column}
	\begin{column}{0.5\textwidth}
\begin{center}
\begin{block}{Output}
	\begin{itemize}
		\item Item 1
		\item Item 2
		\begin{itemize}
			\alert{\item Sub-item 1}
			\item Sub-item 2
		\end{itemize}
	\end{itemize}
\end{block}
\end{center}
	\end{column}
\end{columns}
\end{frame}

%-----------------------------------------------------------------------------------
\begin{frame}[fragile]{Numbered lists}
\begin{columns}[t]
	\begin{column}{0.5\textwidth}
\begin{codeblock}{1}{\LaTeX\ code}
\begin{verbatim}
\begin{enumerate}
  \item Item 1
  \item Item 2
  \begin{enumerate}
     \alert{\item Sub-item 1}
     \item Sub-item 2
  \end{enumerate}
\end{enumerate}
\end{verbatim}
\end{codeblock}
	\end{column}
	\begin{column}{0.5\textwidth}
\begin{center}
\begin{block}{Output}
	\begin{enumerate}
		\item Item 1
		\item Item 2
		\begin{enumerate}
			\alert{\item Sub-item 1}
			\item Sub-item 2
		\end{enumerate}
	\end{enumerate}
\end{block}
\end{center}
	\end{column}
\end{columns}
\end{frame}

%-----------------------------------------------------------------------------------
\begin{frame}[fragile]{Colors and highlighting}
	\begin{itemize}
		\item The LTU add-on provides two colors:
		\begin{enumerate}
			\item \texttt{ltublue}
			\alert{\item \texttt{ltured}}
		\end{enumerate}
		\item The default body text color is \texttt{ltublue} but you can change these locally by using the \texttt{$\backslash$textcolor} command.
		\item The next slide shows how.
	\end{itemize}
\end{frame}

%-----------------------------------------------------------------------------------
\begin{frame}[fragile]{Colors and highlighting}
\begin{columns}[t]
	\begin{column}{0.6\textwidth}
\begin{codeblock}{1}{\LaTeX\ code}
\begin{verbatim}
	\textcolor{ltublue}{Blue text}\\
	\textcolor{ltured}{Red text}
\end{verbatim}
\end{codeblock}
	\end{column}
	\begin{column}{0.4\textwidth}
\defineblockdefaultsLTU
\begin{center}
\begin{block}{Output}
\textcolor{ltublue}{Blue text}\\
\textcolor{ltured}{Red text}
\end{block}
\end{center}
	\end{column}
\end{columns}
\end{frame}

%-----------------------------------------------------------------------------------
\begin{frame}{Pausing lists}
\begin{itemize}
	\item Pausing display between items in lists are simple [Press any key]
	\pause
	\item The code for doing this is shown on the next slide.
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Pausing lists}
\begin{codeblock}{.75}{\LaTeX\ code}
\begin{verbatim}
  \begin{itemize}
     \item Item 1
     \pause
     \item Item 2
     \pause
     \item Item 3
  \end{itemize}
\end{verbatim}
\end{codeblock}
\end{frame}

%-----------------------------------------------------------------------------------
\subsection{Adding graphics}
\begin{frame}{Adding graphics}
	\begin{itemize}
		\item Two custom-made functions for including images are included in this template
		\begin{enumerate}
			\item \texttt{$\backslash$scaledimage} -- Automatically scales and displays an image so that it fits the slide.
			\item \texttt{$\backslash$imagebox} -- Same as above, but this function places the figure inside a box (thus covering the slide background graphics).
		\end{enumerate}
		\item Including graphics manually is done with the \texttt{$\backslash$includegraphics} command.
		\item Positioning of the figure can be done manually using the \texttt{$\backslash$putfig} or the \texttt{$\backslash$putimageblock} command, explained later.
		\item The next few slides will show some\\ simple examples.
	\end{itemize}
\end{frame}

%-----------------------------------------------------------------------------------
\begin{frame}{The \texttt{$\backslash$scaledimage} command}
	\scaledimage{peaks.pdf}
\end{frame}

%-----------------------------------------------------------------------------------
\begin{frame}{The \texttt{$\backslash$imageblock} command}
	\imageblock{peaks.pdf}
\end{frame}

%-----------------------------------------------------------------------------------
\begin{frame}{Position a figure manually}
A new command has been included that helps positioning figures manually.
\begin{itemize}
	\item The command is \texttt{$\backslash$putfig\{x\}\{y\}\{width\}}
	\item \texttt{x, y} and \texttt{width} are all to be specified in fractions of the page size.
	\item The command \texttt{$\backslash$putimageblock\{x\}\{y\}\{width\}} will do the same, but with the figure enclosed in a shadowed box.
\end{itemize}
\end{frame}

%-----------------------------------------------------------------------------------
\begin{frame}[fragile]{Position a figure manually}
\begin{codeblock}{1}{\LaTeX\ code}
\begin{verbatim}
 % Place a figure with the bottom left corner 
 % at the center of the bottom left quadrant of 
 % the slide. Scale the width of the figure to 
  % 15% of the slide width
 \putfig{.25}{.75}{.20}{peaks.pdf}
\end{verbatim}
\end{codeblock}
\putfig{.25}{.25}{.20}{peaks.pdf}
\end{frame}

%-----------------------------------------------------------------------------------
\begin{frame}[fragile]{Position a figure manually}
\begin{codeblock}{1}{\LaTeX\ code}
\begin{verbatim}
 \putimageblock{.55}{.95}{.25}{peaks.pdf} 
 \putimageblock{.35}{.80}{.25}{peaks.pdf}
 \putimageblock{.15}{.65}{.25}{peaks.pdf}
\end{verbatim}
\end{codeblock}
The result is shown on the next slide.
\end{frame}

%-----------------------------------------------------------------------------------
\begin{frame}{Position a figure manually}
\putimageblock{.55}{.90}{.25}{peaks.pdf}
\putimageblock{.35}{.80}{.25}{peaks.pdf}
\putimageblock{.15}{.65}{.25}{peaks.pdf}
\end{frame}

%-----------------------------------------------------------------------------------
\section{Columns and overlays}
\begin{frame}{Multiple columns}
\begin{itemize}
	\item The Beamer document class provides a multi-column environment, so that the page can be split into two or more columns.
	\item The following code will create two columns, one with a bullet list and one with a figure.
\end{itemize}
\end{frame}

%-----------------------------------------------------------------------------------
\begin{frame}[fragile]{Multiple columns}
\begin{codeblock}{.8}{\LaTeX\ code, two columns}
\begin{verbatim}
\begin{columns}[t]
  \begin{column}{0.5\textwidth}
    \begin{itemize}
       \item Some text
       \item A figure to the right.
    \end{itemize}
  \end{column}
  \begin{column}{0.5\textwidth}
     \scaledimage{peaks.pdf}
  \end{column}
\end{columns}
\end{verbatim}
\end{codeblock}
\end{frame}

%-----------------------------------------------------------------------------------
\begin{frame}{Multiple columns}
\begin{columns}[t]
\begin{column}{0.5\textwidth}
  \begin{itemize}
    \item Some text
	\item A figure to the right.
  \end{itemize}
\end{column}
\begin{column}{0.5\textwidth}
  \scaledimage{peaks.pdf}
\end{column}
\end{columns}
\end{frame}
%-----------------------------------------------------------------------------------
\section{Equations}
\begin{frame}{Equations}
Equations are written as in standard \LaTeX\ or using the \texttt{eqblock} environment.
\[
f(\mathbf{x}) = \sum_{n=1}^{N-1} \frac{1}{x_n}
\]
\begin{eqblock}{.8}{A simple boxed equation}
	f(\mathbf{x}) = \sum_{n=1}^{N-1} \frac{1}{x_n}
\end{eqblock}
\end{frame}

%-----------------------------------------------------------------------------------
\section{Slide notes and handouts}
\begin{frame}{Slide notes and handouts}
	\begin{itemize}
		\item With Beamer you can make notes to yourself.
		\item These are only printed if the document is typeset using the \texttt{shownotes} class option
		\item Handouts with space for notes are generated with the class option \texttt{handouts}.
		\item Handouts and notes can not be active at the same time.
	\end{itemize}
	\note[item]{Note to item 1}
	\note[item]{Do not forget what to say!}
\end{frame}

%-----------------------------------------------------------------------------------
\begin{frame}{Example of handout view}
\imageblock{handout_example.pdf}
\end{frame}


%-----------------------------------------------------------------------------------
\section{Additional features}

\begin{frame}{Additional features}
\begin{itemize}
	\item A couple of additional commands and environments have been included in the LTU add-on to Beamer.
	\begin{itemize}
		\item \texttt{codeblock} -- A boxed environment for displaying programming examples.
	\end{itemize}
	\item See the source of this document, and the following slides for example of how to use these features.
\end{itemize}
\end{frame}

%-----------------------------------------------------------------------------------
\begin{frame}[fragile]{The code block}
	\begin{codeblock}{.75}{Code block example}
		$\backslash$begin\{codeblock\}\{.75\}\{Code block example\}\\
		 \hspace{.5cm}$\backslash$begin\{verbatim\}\\
		 \hspace{1cm} \{ Your code here \}\\
		 \hspace{.5cm}$\backslash$end\{verbatim\}\\
        $\backslash$end\{codeblock\}
    \end{codeblock}
  \alert{NOTE: In order to use the \texttt{verbatim} environment, the\\
 frame has to be declared with the \texttt{[fragile]} option.}
\end{frame}
%-----------------------------------------------------------------------------------
\end{document}