# Monday 2nd September

## Session 1: Theme C

### Presentation 1
- Evapotranspiration modelling in high peruvian andeas
- Remote sensor techniques
- Presentation in spanish.....
- *not interesting for me*

### Presentation 2
- computational modelling for driftwood collision dynamics in shallow flows
- new method compared with DEM and non collision driftwood model
- 2D curvilinear solver for flow and lagrangian solver for driftwood
- Anisotropic bed friction, I guess this is mainly important for the particles (driftwood)
- *could be interesting*

### Presentation 3
- stochastic model of daily precipitation
- north atlantic oscilation index
- rainfall in andalusia is negatviely correlated to the north atlantic oscilation index, this is a study of a precipitation model with a lag
*not interesting for me*

### Presentation 4
- estimation of maize evapotranspiration
- sebal algorithm
- really bad english
- *not interesting for me*

### Presentation 5
- The role of waves on flow resistance in high speed shear flow
- very advanced
- energy dissipation due to shock waves
- *could be interesting to read the article*

## Session 2: Theme C

### Presentation 1
- experimental results of gravity currents traveling over a fissured bed
- experimental investigation of how for instance higher density polutants spread in rivers, river bed is considered as a porus media
- not so applied, contains many plots about turbulence and densities etc
- some similarities to robins work

### Presentation 2
- satelite imagery to detect spatial temporal changes in bodies of water
- maybe google earth engine can be used somehow for me, maybe for hydrological models if I am interested to do that for "natural flows"
- *Possibly interesting if I am interested in hydrological stuff*

### Presentation 3
- Devoloping flushing flow functions for gravel bed rivers
- flushing flow is the flow where the flow will start flushing gravel and similar stuff downstream
- the functions take several flow parameters into consideration in order to find if a flushing flow is occuring
- probably needs a lot of sediment data
- *Flushing flows could be of interest for me*

### Presentation 4
- numerical simulation of temperature distribution in tropical deep reservoir
- Delft3D was used with delft heat flux model, the pictures are verbatim taken from the modelling guide :)
- Maybe read this article just to see the modelling of the heat flux and temperature
- Maybe the upstream bathymetry is completely available? I think Gunnar did CFD in the entire reservoir
- *Probably of interest if I have to model the temperature in the upstream reservoir*

### Presentation 5
- Seismic analysis by groundwater fluctuations and precipatation and gnss data in south korea
- Presenter not here

### Presentation 6
- effect of water depth and ice thickness of ice cover blasting for ice jam flood prevention
- ice dynamics on rivers
- *I don't think it's relevant for me but it's an interesting topic*

## Session 3: Special Session 3
Experimental validation of vegetation flow resistance
### Presentation 1
- large scale outdoor flume experiments for interactions between flow, sediment and vegetation
- experiments for understanding the influence of vegetation
- maybe of relevance, in my case I don't think I have a lot of vegetation in the river
- cool experiments with real vegetation
- Study of willow influence on turbulence
- Incredible experiment setup
- *Can be relevant, should read this article*

### Presentation 2
- measurement of lateral and wake flows associated with stream scale willow patches
- vegetation movement in the flow significantly influences the wake properties and the turbulent properties
- *Can be relevant, should read this article*

### Presentation 3
- Validation of global flow resistance models in two experimental drainage channels covered by common reed
- experimental values of chezy coefficients for reeds
- *Maybe relevant if I want to model reeds*

### Presentation 4
- The effect of slope and ramp length on the upstream passage performance of potamodromous cyprinidis negotiating low head ramped weirs
- study of how the angle and lenght of ramp affects the performance of iberian barbels ability to migrate upstream
- *probably not interesting to me*

### Presentation 5
- typical biological behavior of migration and eco-hydraulics of fish schooling
- 3600 meter fishway!
- biological particle model
- *could be interesting*

### Presentation 6
- Experimental studies on levee stability with new eco-friendly materials
- trying to find materials to replace cement
- *Probably not interesting for me*


## Notes and Ideas
- How does the reservoir temperature vary with start and stop of the turbines?
- Is there inflow data available to the reservoir in Stornorrfors?
- Is it possible to do a coupled simulation of the reservoir and the downstream reach? Maybe a simulation can be done for the reservoir to study the temperature as a function of the seasons.
