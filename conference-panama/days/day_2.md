# Tuesday 3rd September

## Session 1: Theme E
Session with sediment transport and river morphology
### Presentation 1
- development and application of calculation method for amount of suspended sediment entrainment under non-equilibrium conditions of flows and sediment transport
- small scale simulations for sediments
- *not so interesting for me*

### Presentation 2
- incipient motion criteria of the uniform gravel bed under falling spheres and natural stones impact in open channel flows
- stuff about when sediment starts moving in rivers, bed shear stress, critical shear stress
- *not interesting before I start studying sediment*

### Presentation 3
- firefly optimization algorithm effect on adaptive neuro fuzzy inference systems prediction improvement of sediment transport in sewer systems
- firefly algorithm
- *not interesting before I study sediment*

### Presentation 4
- estimation of river bend migration rate for a meandering river by using tree ring analysis
- using tree rings to decide how quickly a meandering river has changed
- *not interesting for me*

### Presentation 5
- hydrodynamic characterization of gravel river flows: influence of bed hydraulic conductivity
- characterization of scales
- similar to what robin was working with
- *not interesting for my current work*

### Presentation 6
- kevins presentation
- river bed downcutting equilibrium of a reach of the yangtze river
- "hungry" river
- *probably not interesting for me*

## Session 2: Theme C

### Presentation 1
- movable bed physical model yields replacing a clogged fish pass
- sediment depositions in fishways
- *probably not interesting*

### Presentation 2
- on the undular hydraulic jump and undular surge
- *speaker didn't turn up*

### Presentation 3
- hydraulics of horizontal bar racks for fish downstream migration
- detailed investigation of hydraulics of horizontal bar guidance racks
- *probably not interesting for me*

### Presentation 4
- hydraulic design of fish friendly cost effective box culverts using hybrid modelling for better design guidelines
- chansons presentation
- optimization of box culverts for fish passage
- *probably not interesting*

### Presentation 5
- hydraulic and fish biological performance of fish guidance structures with curved bars
- downstream fish guiding with vertical bars curved bars
- *probably not interesting*

## Session 3: Theme C

### Presentation 1
- Eel and salmon downstream migration along the meuse river
- detection of eel and salmon migration
- *possibly interesting in the future for me*

### Presentation 2
- design of floating fish guidance systems using cfd modelling
- similar to the work gunnar did in stornorrfors
- *possibly interesting but probably not*

### Presentation 3
- a biological turbulence intensity index for pacific lamprey passage of artifical fishways
- suggested biological index for lamprey
- *not very interesting for me*

### Presentation 4
- fish passage hydrodynamics validation of an at scale experimental investigation
- *probably not interesting for me*

### Presentation 5
- describing the daily vertical migration of a blooming freshwater dinoflagellate and simulating the bloom by coupling migration equation into ecological model
- *probably not relevant for me*

### Presentation 6

# Notes and Ideas
- Maybe firefly algorithm or something similar can be used in the paper where I want to find a function of damping and water level to find an optimized function, mathematical optimization in wikipedia
