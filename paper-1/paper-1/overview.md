# Paper 1 - Case Study of Damping Properties in Stornorrfors Bypass Reach
- Continuation of conference paper presented at Delft.
- Preliminary title - "Case Study of Damping Properties in Stornorrfors Bypass Reach"
## Previous work
- Conference paper Panama.
- Conference paper Delft.

## Content of this paper
- Mesh study
  - Global variable wetted area
  - local variable water level
- Richardson extrapolation
- Continue with results from conference paper
  - Normalized water levels
  - Actual water levels
  - Wetted area
  - Manning distribution vs Uniform Manning
- Froude (I need some new angle of discussion if this should be included)
- "Hysteresis" effect in water level and discharge when flow is increased or decreased respectively
- Other suggestions (Anders, Gunnar and Patrik)

## Hydraulics
- Use Delft3D for hydraulic modelling
- Roughness Calibration
  - Use the 51 -> 20 m³/s case for calibration and validation (Panama conference)
  - Data Assimilation with OpenDA (not been successful so far)

## Structure
  - Abstract
  - Introduction
  - Method
    - Area
    - Manning vs Uniform
    - Mesh
      - Mesh study
      - Richardson
        - Water level in validation points
        - Wetted area
  - Results and Discussion
    - Inherent Damping
      - Water levels (new better plots compared to delft conference)
      - (Discharge vs depth in time)
    - Dynamics
      - Wetted area
      - How does start / stop frequency affect
        - Use "downstream steady state" for each start / stop scheme to compare with normal steady state as a measurement, how much is the downstream steady state affected?
        - Define a point where the "downstream steady state" occurs, this can be used to compare different spilling schemes
      - Hysteresis?

## Simulations to run
  - Decrease in discharge (250 -> 21 m^3/s)
    - 250 -> 21 in 1 minute *done*
    - 250 -> 21 in 5 minutes *done*
    - 250 -> 21 in 15 minutes *done*
    - 250 -> 21 in 30 minutes *done*
    - 250 -> 21 in 35 minutes (to fill in region of interest in transient area) *done*
    - 250 -> 21 in 40 minutes (to fill in region of interest in transient area) *done*
    - 250 -> 21 in 45 minutes *done*
    - 250 -> 21 in 60 minutes *done*
  - Increase in discharge (21 -> 250 m^3/s)
    - 21 -> 250 in 1 minute *done*
    - 21 -> 250 in 5 minutes
    - 21 -> 250 in 15 minutes *done*
    - 21 -> 250 in 30 minutes *done*
    - 21 -> 250 in 45 minutes *done*
    - 21 -> 250 in 60 minutes *done*
    - more if region of interests arise
    - Decrease in discharge (50 -> 21 m^3/s)
      - 50 -> 21 in 1 minute *done*
      - 50 -> 21 in 5 minutes *done*
      - 50 -> 21 in 15 minutes *done*
      - 50 -> 21 in 30 minutes *done*
      - 50 -> 21 in 35 minutes (to fill in region of interest in transient area)
      - 50 -> 21 in 40 minutes (to fill in region of interest in transient area)
      - 50 -> 21 in 45 minutes *done*
      - 50 -> 21 in 60 minutes *done*
    - Increase in discharge (21 -> 50 m^3/s)
      - 21 -> 50 in 1 minute *done*
      - 21 -> 50 in 5 minutes *done*
      - 21 -> 50 in 15 minutes *done*
      - 21 -> 50 in 30 minutes *done*
      - 21 -> 50 in 45 minutes *done*
      - 21 -> 50 in 60 minutes  *done*
      - more if region of interests arise  
  - Redo simulation with calibrated manning number with good initial conditions
    - 50 - 21 in 1 minute
    - 50 - 21 in 5 minutes
    - 50 - 21 in 15 minute
    - 50 - 21 in 30 minutes
Genom dammluckan
0 m3/s

    - 50 - 21 in 45 minutes
    - 50 - 21 in 60 minutes
    - 21 - 50 in 1 minute
    - 21 - 50 in 15 minutes
    - 21 - 50 in 30 minutes
    - 21 - 50 in 45 minutes
    - 21 - 50 in 60 minutes
  - Simulations that investigate the damping wave with different starts/stops
    - 1 stop per day (baseline)
    - 10 stops per day (every 144 minute)
    - 20 stops per day (every 72 minute)
    - 30 stops per day (every 48 minute) *done*


## Plots
- Hysteresis in validation points


## References that needs to be found
  - Reference(s) regarding slowing down the turbine operations
    - how slowly is it possible to reduce the operation of the turbines.
    - how quickly a spillway can be opened (ridas)
  - References about Delft3D
    - See conference papers
  - References about Hydraulics
    - See conference papers
    - References about inherent damping, damping waves etc
    - References about hysteresis
  - References about previous hydraulic work in Stornorrfors
    - See conference papers for references to  Gunnar and Anders
  - References about previous work in Ume river (if relevant)
