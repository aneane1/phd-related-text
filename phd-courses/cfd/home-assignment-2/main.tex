\documentclass[11pt]{scrartcl}

\usepackage{enumitem}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{float}
\usepackage{listings}
\usepackage{pythonhighlight}
\usepackage[top=2.5cm, bottom=2.5cm, left=2.5cm, right=2.5cm]{geometry}
\title{\textbf{Home Assignment 2 - CFD PHD Course}}
\author{Anton Burman}
\date{}
\begin{document}

\maketitle

\section{Problem Statement}
The governing equation for two-dimensional convection and diffusion is 
\begin{equation}
	\frac{\partial \left(\rho u \phi\right)}{\partial x}  + \frac{\partial \left(\rho v \phi\right)}{\partial y} = \Gamma \left(\frac{\partial^2 \phi}{\partial x^2} + \frac{\partial^2 \phi}{\partial y^2}\right)
\end{equation}
where 
\begin{equation}
	u = 1, ~ v = 0.5, ~ \Gamma = 0.025, ~ \rho = 1.
\end{equation}
We are considering a quadratic area of size 1x1 with boundary conditions
\begin{equation}
	\phi(x,0) = 0,~ \phi(0,y) = 100, ~\phi(x,1) = 0, ~\phi(1,y) = 0.
\end{equation}
The task of this assignment is to use the finite volume method with the following interpolation schemes to solve equation 1 with the boundary conditions in equation 3. The interpolation schemes are 
\begin{enumerate}
	\item Central Differencing Scheme (CDS)
	\item Upwind Differencing Scheme (UDS)
	\item LUI Scheme
\end{enumerate}
\section{Finite Volume Method}
By using vector calculus identities equation 1 can be rewritten as
\begin{equation}
	\nabla \cdot (\phi \mathbf{u}) = \Gamma \nabla (\cdot \nabla(\phi)).
\end{equation}
Integrating equation 4 over an arbitrary control volume gives
\begin{equation}
	\iiint_{cv} \nabla \cdot (\phi u)~dV = \iiint_{cv} \nabla (\cdot \nabla(\phi))~dV
\end{equation}
Using the divergence theorem, the control volume integrals in equation 5 becomes flux integrals across the control volumes surface
\begin{equation}
	\iint_{S} \phi\mathbf{u}\hat{n}~dS = \iint_{S} \Gamma \nabla \phi \hat{n}~dS
\end{equation}
where $\hat{n}$ is the normal vector of the surface and $\mathbf{u} = u\hat{x} + v\hat{y}$. The integral on the LHS is the convective term and the integral on the RHS is the diffusive term. In this report all surface integrals will be approximated using the midpoint rule
\begin{equation}
	F_e = \int_{S_e} f~dS_e \approx f_e S_e
\end{equation}
where $S_e$ is one of the control volume's faces see page 84 in \cite{ferziger2019}. The mid point rule is second order accurate which can be shown by Taylor expanding equation 7 about the cell face center. There are other ways of approximating the integrals, such as the trapezoidal rule, but these methods will not be taken into consideration here. Since the governing equation in this case is two dimensional the control volume reduces to a control area, a schematic of the control volume and its neighbours can be seen in figure 1.
\begin{figure}[H]
	\centering
	\includegraphics[scale=1]{control_volume.jpg}
	\caption{Arbitrary two dimensional square control volume (red) with neighbours.}
\end{figure}
The lowercase letters in red denote the midpoint of each respective face while the uppercase letters in black denotes the midpoint in the volume. For the two dimensional case considered here we can discretize the integrals in equation 6 as
\begin{equation}
	F_s^c + F_w^c + F_n^c + F_e^c = F_s^d + F_w^d + F_n^d + F_e^d
\end{equation}
where $c$ denotes the convective terms, $d$ the diffusive terms and the lowercase letters the respective face of the control volume. The idea is to express the flux across the faces in terms of the neighbouring midpoints. If we assume that the grid size in the $x$ and $y$ direction are the same $\Delta x = \Delta y = \Delta$, the diffusive terms can be approximated by
\begin{equation}
	F_i^D = \Gamma \frac{\partial \phi}{\partial x}\Delta y \approx \Gamma \frac{\Delta y}{\Delta x}\left(\phi_i - \phi_P\right) = \Gamma\left(\phi_i - \phi_P\right), ~i=e,n,w,s
\end{equation}
 By using equation 7 we get the expression for the convective flow through each face
\begin{equation}
	F_i^c = \dot{m}_i \phi_i, ~i=e,n,w,s
\end{equation}
The mass flow $\dot{m}_i$ through each face is given by 
\begin{equation}
\dot{m}_i =
\begin{cases}
u \Delta \hat{x}, ~ \hat{n} = \hat{x} \\
v \Delta\hat{y}, ~\hat{n} = \hat{y}
\end{cases}~i=e,n,w,s     
\end{equation}
We identify that in equation 9 and equation 10 we have the term corresponding to the control volume face, $\phi_i$. The flux across the faces is hence strongly related to how we choose to define this term. A general equation for finding some interpolation schemes can be found on page 93 in \cite{ferziger2019}. The equation for the east face of a control volume is as follows
\begin{equation}
	\phi_e = \phi_P + \left[\frac{1+\kappa}{4}\left(\phi_E - \phi_P\right) + \frac{1-\kappa}{4}\left(\phi_P - \phi_W\right)\right]
\end{equation}
where $-1\le \kappa \le 1$ can be used to extract schemes. In the following sections we will look into three different interpolation schemes for $\phi_i$.
\subsection{Central Differencing Scheme}
The central differencing scheme (CDS) can be extracted from equation 12 by setting $\kappa = 1$. This yields the following relations
\begin{equation}
	\phi_i = 
	\begin{cases}
	\phi_e = \frac{\phi_P + \phi_E}{2} \\
	\phi_n = \frac{\phi_P + \phi_N}{2} \\
	\phi_w = \frac{\phi_P + \phi_W}{2} \\
	\phi_s = \frac{\phi_P + \phi_S}{2} \\
	\end{cases} .
\end{equation}
Using these relations in conjunction with equation 8, 9 and 10 yields 
\begin{equation}
	\phi_P(4\Gamma) + \phi_E\left(\frac{u\Delta}{2}-\Gamma\right) + \phi_N\left(\frac{v\Delta}{2}-\Gamma\right) + \phi_W\left(-\frac{u\Delta}{2} -\Gamma\right) + \phi_S\left(-\frac{v\Delta}{2} -\Gamma\right) = 0.
\end{equation}
From equation 14 we can identify the 5 coefficients that span the computational molecule
\begin{equation}
A_P = 4\Gamma,~ A_E = \frac{u\Delta}{2}-\Gamma, ~A_N =\frac{v\Delta}{2}-\Gamma, ~A_W = -\frac{u\Delta}{2} -\Gamma,~ A_S=-\frac{v\Delta}{2} -\Gamma
\end{equation}
so that
\begin{equation}
	A_P\phi_P + A_E\phi_E + A_N\phi_N + A_W\phi_W + A_S\phi_S = 0
\end{equation}
\subsection{Upwind Differencing Scheme}
The upwind differencing scheme (UDS) is not a $\kappa$ scheme, rather it is obtained by taking the upstream node. This means that
\begin{equation}
\phi_i = 
\begin{cases}
\phi_e = \phi_P \\
\phi_n = \phi_P \\
\phi_w =\phi_W\\
\phi_s =\phi_S\\
\end{cases} .
\end{equation}
Again using equations 8, 9 and 10 we get
\begin{equation}
	\phi_P(v\Delta + u\Delta 4\Gamma) + \phi_E\left(-\Gamma\right) + \phi_N\left(-\Gamma\right) + \phi_W\left(-u\Delta -\Gamma\right) + \phi_S\left(-v\Delta -\Gamma\right) = 0.
\end{equation} 
Then the coefficients for the computational molecule for the UDS scheme becomes
\begin{equation}
A_P = v\Delta + u\Delta + 4\Gamma,~ A_E = -\Gamma, ~A_N = -\Gamma, ~A_W = -u\Delta -\Gamma,~ A_S=-v\Delta -\Gamma.
\end{equation}
\subsection{LUI Scheme}
The LUI scheme is obtained when setting $\kappa=-1$ in equation 12. This gives the following relations
\begin{equation}
\phi_i = 
\begin{cases}
\phi_e = \frac{3\phi_P - \phi_W}{2} \\
\phi_n = \frac{3\phi_P - \phi_S}{2} \\
\phi_w = \frac{3\phi_W - \phi_{WW}}{2} \\
\phi_s = \frac{3\phi_S - \phi_{SS}}{2} \\
\end{cases} .
\end{equation}
As for the other schemes we use equations 8, 9 and 10 which for LUI yields
\begin{multline}
\phi_P\left(4\Gamma +\frac{3 u\Delta + v\Delta}{2}\right) + 
\phi_E\left(-\Gamma\right) + 
\phi_N\left(-\Gamma\right) + 
\phi_W\left(-2u\Delta -\Gamma\right) + 
\phi_S\left(-2v\Delta -\Gamma\right) + \\
+\phi_{SS}\left(\frac{v\Delta}{2}\right) +
\phi_{WW}\left(\frac{u\Delta}{2}\right) = 0.
\end{multline}
We notice that the LUI scheme gives rise two more nodes in the computational molecule, the molecules coefficients are then
\begin{multline}
A_P = 4\Gamma +\frac{3 u\Delta + v\Delta}{2}, ~
A_E = -\Gamma, ~
A_N = -\Gamma, ~
A_W = -2u\Delta -\Gamma,~ 
A_S =- 2v\Delta -\Gamma, ~\\
A_{SS} = \frac{v\Delta}{2},~
A_{WW} = \frac{u\Delta}{2}
\end{multline}
\subsection{Deferred Correction}
In order to have a well behaved computational molecule one can use the method called \textit{deferred correction}. The methods includes putting all components of the computational molecule that are not on the tri-diagonal on the RHS in the linear system of equations. By doing this one can solve a tri-diagonal system iteratively rather than solving a more complex computational molecule. In this case we construct a matrix that consists of both a UDS matrix and a LUI matrix. This means that we can make a first order approximation of the boundary conditions quite conveniently. By subtracting the LUI coefficients in the computational molecule from the UDS coefficients we get the following tri-diagonal computational molecule
\begin{equation}
A_P^*\phi_P + A_E^*\phi_E + A_N^*\phi_N + A_W^*\phi_W + A_S^*\phi_S = A_ {SS}^{LUI}\phi_{SS} + A_ {WW}^{LUI}\phi_{WW}
\end{equation}
where 
\begin{multline}
A_P^* = A_P^{UDS} - A_P^{LUI}, ~
A_E^* = A_E^{UDS} - A_E^{LUI}, ~
A_N^* = A_N^{UDS} - A_N^{LUI}, ~
A_W^* = A_W^{UDS} - A_W^{LUI},~ 
A_S^* =A_S^{UDS} - A_S^{LUI}
\end{multline}.
In this case the equation to be solved iteratively is equation 23. Given an initial guess for $\phi_{SS}$ and $\phi_{WW}$ it is possible to solve equation 23 by using the scheme
\begin{equation}
	F_e^{new} = F_e^{UDS} + \left(F_e^{LUI}-F_e^{UDS}\right)^{old}
\end{equation}
where the newest best approximation for $\phi_{SS}$ and $\phi_{WW}$ is used, see page 146 in \cite{ferziger2019}.
\section{Results}
\subsection{UDS and CDS Schemes}
\begin{figure}[H]
	\centering
	\includegraphics[scale=0.75]{uds_delta_10.png}
	\caption{UDS scheme with $\Delta = 1/10$.}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[scale=0.75]{cds_delta_10.png}
	\caption{CDS scheme with $\Delta = 1/10$.}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[scale=0.75]{uds_delta_50.png}
	\caption{UDS scheme with $\Delta = 1/50$.}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[scale=0.75]{cds_delta_50.png}
	\caption{CDS scheme with $\Delta = 1/50$.}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[scale=0.75]{uds_delta_100.png}
	\caption{UDS scheme with $\Delta = 1/100$.}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[scale=0.75]{cds_delta_100.png}
	\caption{CDS scheme with $\Delta = 1/100$.}
\end{figure}
From the figures above we can conclude that the UDS scheme is unconditionally stable. The CDS scheme, however produces oscillations for the rougher grids. For $\Delta=0.1$ the UDS scheme has a smooth solution, however not so accurate. The CDS scheme oscillates wildly for this case. When we consider $\Delta=0.02$ the UDS scheme looks more converged as we would expect, the CDS is still oscillating. It's first at $\Delta=0.01$ that the CDS scheme is producing realistic results. It is worth noting that the boundary condition at the upper wall causes a huge gradient close to the boundary. This is particularly problematic for the CDS solution.
\subsection{LUI and Deferred Correction}
TO BE IMPLEMENTED.
\section{Python Code}
\inputpython{/home/anton/phd-related-code/cfd_course/assignment_2/assignment_2.py}{1}{200}
\begin{thebibliography}{9}
	
	\bibitem{ferziger2019}
	Joel H Ferziger,
	Milovan Perić,
	Robert L. Street
	\textit{Computational Methods for Fluid Dynamics},
	Springer Nature, Switzerland,
	4th edition,
	2020.
	
	
\end{thebibliography}
\end{document}
