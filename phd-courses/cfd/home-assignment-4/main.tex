\documentclass[11pt]{scrartcl}

\usepackage{bm}
\usepackage{enumitem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{float}
\usepackage{listings}
\usepackage{siunitx}
\usepackage{pythonhighlight}
\usepackage[top=2.5cm, bottom=2.5cm, left=2.5cm, right=2.5cm]{geometry}

\newcommand{\R}{\mathbb{R}}
\title{\textbf{Home Assignment 4 - CFD PHD Course}}
\author{Anton Burman}
\date{}
\begin{document}

\maketitle

\section{Problem Statement}
The transient 1D convection diffusion equation is
\begin{equation}
\frac{\partial \phi}{\partial t} = -u \frac{\partial \phi}{\partial x} + \frac{\Gamma}{\rho}\frac{\partial^2\phi}{\partial x^2}
\end{equation} 
where the given initial condition is
\begin{equation}
	\phi(0, x) =
	\begin{cases}
	0, ~0<x<L\\
	1, ~x=L
	\end{cases}
\end{equation}
and the boundary conditions are
\begin{equation}
\begin{cases}
\phi(t, 0) = 0\\
\phi(t, 1) = 1
\end{cases}
\end{equation}
and the physical parameters are
\begin{equation}
u = 1, ~ \Gamma = 0.1, ~ \rho = 1.
\end{equation}
The purpose of this assignment is to investigate the behavior of the following time integration methods
\begin{itemize}
	\item Euler forward method
	\item Third order Runge-Kutta method (Heun's method)
	\item Multi step Adams-Bashfort-Moulton solver (ode113) in MATLAB
\end{itemize}
The spatial discretization will be a 2nd order CDS method.
\section{Derivation of Equations}
\subsection{Spatial Discretization}
In this exercise the same spatial discretization scheme will be used for all time integration methods. The chosen method is the central difference method described in home assignment 1.
The approximate derivatives are then
\begin{equation}
\frac{\partial^2\phi}{\partial x^2} = \frac{\phi(x-\Delta x) - 2\phi(x) + \phi(x +\Delta x)}{\Delta x^2} + \mathcal{O}(\Delta x^{3})
\end{equation}
\begin{equation}
\frac{\partial\phi}{\partial x} = \frac{\phi(x + \Delta x) - \phi(x - \Delta x)}{2\Delta x}  + \mathcal{O}(\Delta x^{3})
\end{equation}
given that there is a uniform grid-spacing $\Delta x$. Inserting equation 4 and 5 in equation 1 and truncating terms smaller than $\mathcal{O}(\Delta x^{2})$ yields
\begin{equation}
\frac{\partial \phi}{\partial t} = -u \left(\frac{\phi(x + \Delta x) - \phi(x - \Delta x)}{2\Delta x}\right) + \frac{\Gamma}{\rho} \left(\frac{\phi(x-\Delta x) - 2\phi(x) + \phi(x +\Delta x)}{\Delta x^2}\right).
\end{equation}
Equation 6 can be rearranged as
\begin{equation}
	\frac{\partial \phi}{\partial t} = 
	\phi(x + \Delta x)\left(\frac{-u}{2\Delta x} + \frac{\Gamma}{\rho \Delta x^2}\right) +
	\phi(x)\left(\frac{-2\Gamma}{\rho \Delta x^2}\right) + 
	\phi(x-\Delta x) \left(\frac{u}{2\Delta x} + \frac{\Gamma}{\rho \Delta x^2}\right).
\end{equation}
From equation 7 we identify that the resulting system of equations will be a tri-diagonal system. It can also be seen that the steady state case where the LHS is 0 corresponds to the system in home assignment 1.
\section{Transient Discretizations}
In the cases of the forward Euler method and Huen's method the solution is considered converged when
\begin{equation}
	||\phi^n-\phi^{n-1}||_2 < \num{1e-5}.
\end{equation}
\subsection{Forward Euler Method}
The forward Euler method is obtained by Taylor expanding $\phi$ about a point $\Delta t$
\begin{equation}
	\phi(t + \Delta t) = \phi(t) + \Delta t \frac{\partial \phi}{\partial t} + \mathcal{O}(\Delta t^2).
\end{equation}
We truncate all terms smaller than $\Delta t$ we can rewrite equation 9 with the following notation
\begin{equation}
	\phi^{n+1} = \phi^{n} + \Delta t \frac{\partial \phi^n}{\partial t}
\end{equation}
where $n$ denotes the current timestep and $n+1$ is the next timestep. Here we further identify that the term containing the first derivative of $\phi$ is the RHS in equation 8. Hence the function in the next timestep is only a function of the current timestep. This is referred to as an explicit method. Equation 8 in equation 10 yields
\begin{equation}
	\phi^{n+1}(x) = \phi^{n}(x) + \Delta t\left(
	\phi^n(x + \Delta x)\left(\frac{-u}{2\Delta x} + \frac{\Gamma}{\rho \Delta x^2}\right) +
	\phi^n(x)\left(\frac{-2\Gamma}{\rho \Delta x^2}\right) + 
	\phi^n(x-\Delta x) \left(\frac{u}{2\Delta x} + \frac{\Gamma}{\rho \Delta x^2}\right)\right).
\end{equation}
By introducing the variables
\begin{equation}
	d = \frac{\Gamma \Delta t}{\rho \Delta x^2}
\end{equation}
and
\begin{equation}
	c = \frac{u\Delta t}{\Delta x}
\end{equation}
equation 11 can be written as
\begin{equation}
	\phi^{n+1}(x) = (1-2d)\phi^n(x) + \left(d-\frac{c}{2}\right)\phi^n(x+\Delta x) + \left(d+\frac{c}{2}\right)\phi^n(x-\Delta x)
\end{equation}
The requirement on the timestep in order to have stability is
\begin{equation}
	\Delta t < \frac{\rho \Delta^2}{2\Gamma}.
\end{equation}
With the parameters given above and a spatial resolution $\Delta = 1/41$ we get $\Delta t < 0.002975~s$.
\subsection{Huen's Method}
Huen's method is a third order Runge-Kutta method and consists of three steps.
\begin{itemize}
	\item The first step is the \textit{predictor} step for the first 1/3rd of the step
	\item The second step is a midpoint rule approximation around 2/3rds of the step
	\item The third step is a trapezoidal rule approximation at 3/3rds of the step
\end{itemize}
The initial predictor step is a regular forward Euler step to 1/3rd of the timestep
\begin{equation}
	\phi_{n+1/3}^* = \phi^n + \frac{\Delta t}{3}f(t_n, \phi^n)
\end{equation}
where $\phi^*$ denotes the first approximation of the next timestep and $n$ is the previous timestep. In this implementation the function $f$ corresponds to solving the discretized linear system of equations described in section 2. The second midpoint approximation step is then
\begin{equation}
	\phi_{n+2/3}^* = \phi^n + \frac{2 \Delta t}{3}f(t_{n+2/3}, \phi_{n+1/3}^*).
\end{equation}
Finally the last trapezoidal step approximation is given by
\begin{equation}
	\phi^{n+1} = \phi^n + \frac{\Delta t}{4}\left(f(t_n, \phi^n) + 3f(t_{n+2/3}, \phi_{n+2/3}^*)\right)
\end{equation}
\subsection{Multi step Adams-Bashfort-Moulton Method}
The class of Adam's methods are derived by fitting polynomials to derivatives at different points in time \cite{ferziger2019}. The python package SciPy includes a differential equation solver that implements the "Adams/BDF" method \cite{lsoda}.
\section{Results}

\subsection{Steady State Solutions}
In figure 1 we see the final steady state solutions for the three different methods. The timestep used for the Euler forward method and Huen's method is $\Delta t = 0.002975~s$. The forward Euler method and Huen's method both converge to the reference solution after a sufficient number of time steps. The Adam's method however converges to the same solution as was found in home assignment 1. At this point I have no good explanation of why this happens except that it could be cause by how the solver is implemented in Python. The solver is initialized with a vector of length 40 containing the initial values. This corresponds to solving 40 ordinary differential equations for each time step. Afterwards the boundary conditions are enforced. It is possible that this implementation don't properly catch the physical behavior. In figure 2 we see how the error of the solution behaves for the Euler forward and Huen's method. The shape and magnitude is similar, Huen's method converges a little bit faster however. 
\begin{figure}[H]
	\hspace{-2 cm}
	\includegraphics[scale=1]{steady_state.png}
	\caption{Steady state solutions for the three methods under investigation. Adam's method (left), Euler forward method (center) and Huen's method (right).}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[scale=0.8]{error.png}
	\caption{Comparison of the number of iterations required for the forward Euler and Heun's method to reach steady state. Euler forward (top) and Huen's method (bottom).}
\end{figure}
\subsection{Stability Analysis}
In table 1 the number of timesteps required to reach steady state have been tabulated. At the theoretical stability point for the Euler method it is comparable to Huen's method in performance, for larger timesteps this is not the case however. Huen's method remains stable until $\Delta t = 0.0035$ then it rapidly diverges. 
\begin{table}[H]
	\centering
	\caption{Number of timesteps required to reach steady state for the Euler forward method and Huen's method. Timesteps that resulted in divergence are denoted as (-).}
	\begin{tabular}{l c c c c c c c}
		$\Delta t~[s]$& 0.002975 & 0.002985 &0.00299 & 0.003 & 0.0035 & 0.00375 & 0.0038\\
		\hline \hline 
		Euler Forward       & 992  & 2872 & 96586 & - & - & - & -\\
		Huen's Method       & 942  & 938  & 938 & 935 & 814 & 1335 &-
	\end{tabular}
\end{table}
\section{Python Code}
\inputpython{/home/anton/phd-related-code/cfd_course/assignment_4/assignment_4.py}{1}{200}
\begin{thebibliography}{9}
	
	\bibitem{ferziger2019}
	Joel H Ferziger,
	Milovan Perić,
	Robert L. Street
	\textit{Computational Methods for Fluid Dynamics},
	Springer Nature, Switzerland,
	4th edition,
	2020.
	 \bibitem{lsoda}
	 scipy.integrate.lsoda - SciPy v1.4.1 reference guide, scipy.integrate.LSODA, https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.LSODA.html, scipy.integrate.LSODA - SciPy v1.4.1 Reference Guide 
	
\end{thebibliography}
\end{document}
